#include "fen_object.hh"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <optional>
#include <string>
#include <vector>

#include "color.hh"
#include "fen_rank.hh"
#include "piece-type.hh"
#include "position.hh"

namespace board
{
    inline File char_to_file(char c)
    {
        switch (c)
        {
        case 'a':
            return File::A;
        case 'b':
            return File::B;
        case 'c':
            return File::C;
        case 'd':
            return File::D;
        case 'e':
            return File::E;
        case 'f':
            return File::F;
        case 'g':
            return File::G;
        case 'h':
            return File::H;
        default:
            throw std::invalid_argument("Unknown piece");
        }
    }

    inline Rank char_to_rank(char c)
    {
        switch (c)
        {
        case '3':
            return Rank::THREE;
        case '6':
            return Rank::SIX;
        default:
            throw std::invalid_argument("Unknown piece");
        }
    }

    FenObject::FenObject(FenRank* ranks, Color side_to_move,
                         std::vector<char> castling,
                         std::optional<Position> en_passant_target)
        : side_to_move_(side_to_move)
        , castling_(castling)
        , en_passant_target_(en_passant_target)
    {
        for (unsigned i = 0; i < 8; i++)
            ranks_[i] = ranks[i];
    }

    /*
    FenObject::opt_piece_t FenObject::operator[](board::Position position) const
    {
        Rank rank = position.rank_get();
        FenRank fen_rank = ranks_[utils::utype(rank)];
        return fen_rank[position.file_get()];
    }
    */

    FenObject FenObject::parse(std::vector<std::string> vect)
    {
        std::vector<std::string> ranks;
        FenRank ranks_[8];
        boost::split(ranks, vect[0], boost::is_any_of("/"));
        for (unsigned i = 0; i < ranks.size(); i++)
        {
            ranks_[i] = FenRank(ranks[i]);
        }

        Color side_to_move_ = (vect[1][0] == 'b') ? Color::BLACK : Color::WHITE;

        std::vector<char> castling_;
        if (vect[2][0] != '-')
        {
            for (auto c : vect[2])
            {
                castling_.push_back(c);
            }
        }

        std::optional<Position> en_passant_target_ = std::nullopt;
        if (vect[3][0] != '-')
        {
            en_passant_target_ = {char_to_file(vect[3][0]),
                                  char_to_rank(vect[3][1])};
        }

        return FenObject(ranks_, side_to_move_, castling_, en_passant_target_);
    }
} // namespace board
