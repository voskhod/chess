#include "utype.hh"

namespace board
{
    inline Position::Position(File file, Rank rank)
        : file_(file)
        , rank_(rank)
    {}

    inline Position::Position(char file, char rank)
    {
        file_ = File(static_cast<int>(file) - static_cast<char>('a'));
        rank_ = Rank(static_cast<int>(rank) - static_cast<char>('1'));
    }

    inline bool Position::operator==(const Position& pos) const
    {
        return file_get() == pos.file_get() && rank_get() == pos.rank_get();
    }

    inline bool Position::operator!=(const Position& pos) const
    {
        return !(*this == pos);
    }

    inline File Position::file_get() const
    {
        return file_;
    }

    inline Rank Position::rank_get() const
    {
        return rank_;
    }

    inline std::ostream& operator<<(std::ostream& os, const Position& pos)
    {
        return os << static_cast<char>('a' + utils::utype(pos.file_))
                  << utils::utype(pos.rank_) + 1;
    }
} // namespace board
