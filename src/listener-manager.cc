#include "listener-manager.hh"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <sstream>

#include "ai.hh"
#include "fen_object.hh"
#include "perft-object.hh"
#include "pgn-parser.hh"
#include "uci.hh"

namespace listener
{
    ListenerManager::ListenerManager(const std::vector<std::string>& paths)
    {
        for (const auto& path : paths)
            listeners_.emplace_back(path);
    }

    board::ChessboardInterface::opt_piece_t
    ListenerManager::operator[](const board::Position& position) const
    {
        return board_[position];
    }

    void ListenerManager::get_fen(std::stringstream& ss)
    {
        std::string token;
        ss >> token; /* position */
        ss >> token; /* fen/start pos */

        std::string fen_str = "";

        if (token != "startpos")
        {
            for (ss >> token; token != "moves" && token != ""; ss >> token)
                fen_str += " " + token;
        }

        std::vector<std::string> vect;

        if (fen_str != "" && fen_str != "startpos")
        {
            fen_str = fen_str.substr(1);
            boost::split(vect, fen_str, boost::is_any_of(" "));
            auto fen = board::FenObject::parse(vect);
            board_ = board::Chessboard(fen);
            board_.end_turn();
        } else
            board_ = board::Chessboard();
    }

    bool ListenerManager::get_moves(std::stringstream& ss)
    {
        std::string token;
        ss >> token;
        if (token == "")
            return false;

        std::cerr << token << " " << token[4] << std::endl;

        board::Position from(token[0], token[1]);
        board::Position to(token[2], token[3]);

        board::PieceType piece = board_[from]->first;
        switch (token[4])
        {
        case 'q': {
            piece = board::PieceType::QUEEN;
            break;
        }
        case 'r': {
            piece = board::PieceType::ROOK;
            break;
        }
        case 'b': {
            piece = board::PieceType::BISHOP;
            break;
        }
        case 'n': {
            piece = board::PieceType::KNIGHT;
            break;
        }
        default: {
            break;
        }
        }

        board::Move m(piece, from, to);

        if (piece == board::PieceType::PAWN)
        {
            /* Check it's a double push */
            if ((from.rank_get() == board::Rank::TWO
                 && to.rank_get() == board::Rank::FOUR)
                || (from.rank_get() == board::Rank::SEVEN
                    && to.rank_get() == board::Rank::FIVE))
                m.set_double_pawn_push();

            /* Check en passant */
            else if (m.capture() && !board_[to])
                m.set_en_passant();
        }

        /* Check if it's a castling */
        if (piece == board::PieceType::KING
            && from.file_get() == board::File::E)
        {
            if (to.file_get() == board::File::G)
                m.set_kingside_castling();
            else if (to.file_get() == board::File::C)
                m.set_queenside_castling();
        }

        /* As the uci don't tell if a capture happen, we always set the
         * capture. If there is no piece, the capture will change nothing
         */
        m.set_capture(board::PieceType::PAWN);

        board_.do_move(m);
        board_.end_turn();

        return true;
    }

    void ListenerManager::update_board()
    {
        auto input = ai::get_board();
        std::cerr << input << std::endl;
        std::stringstream ss(input);

        std::cerr << "Parsing fen\n";
        get_fen(ss);

        std::cerr << "Parsing moves\n";
        ss >> input;
        while (get_moves(ss))
            continue;
    }

    void ListenerManager::play_ai()
    {
        ai::init("Piece of shit");

        for (;;)
        {
            std::cerr << board_ << "Waiting input\n";
            update_board();

            std::cerr << "Generate moves\n";
            const board::Move bestmove = ai::search_with_opening(board_);
            ai::play_move(bestmove);

            std::cerr << "Play " << bestmove << "\n\n";
            board_.do_move(bestmove);
            board_.end_turn();
        }
    }

    void ListenerManager::run_pgn(const std::string& path)
    {
        for (const auto& pgn_move : pgn_parser::parse_pgn(path))
        {
            board::Move move = pgn_move.to_move();
            auto full_move = board_.is_legal_move(move);

            if (!full_move)
            {
                on_player_disqualified(board_.get_player());
                on_game_finished();
                break;
            }

            do_pgn_move(*full_move);
        }
    }

    void ListenerManager::run_perft(const std::string& path)
    {
        board::PerftObject perft = board::PerftObject::parse_from_file(path);
        auto count = perft.evaluate(perft.depth_get());
        std::cout << count << std::endl;
    }

    void ListenerManager::close_listeners()
    {
        for (auto& l : listeners_)
            l.close();
    }

    void ListenerManager::do_pgn_move(const board::Move& move)
    {
        on_piece_moved(move.get_piece(), move.from(), move.to());

        if (move.kingside_castling())
            on_kingside_castling(board_.get_player());

        if (move.queenside_castling())
            on_queenside_castling(board_.get_player());

        if (move.en_passant())
        {
            board::Position to_delete(move.to().file_get(),
                                      move.to().rank_get());
            on_piece_taken(board::PieceType::PAWN, to_delete);
        } else if (move.capture())
            on_piece_taken(board_[move.to()]->first, move.to());

        if (move.promotion())
            on_piece_promoted(*move.promotion(), move.to());

        board_.do_move(move);

        bool can_move = board_.can_move();

        if (board_.is_check(board_.get_player()))
        {
            if (!can_move)
            {
                on_player_mat((board_.get_player() == board::Color::WHITE)
                                  ? board::Color::BLACK
                                  : board::Color::WHITE);
                on_game_finished();
            } else
                on_player_check((board_.get_player() == board::Color::WHITE)
                                    ? board::Color::BLACK
                                    : board::Color::WHITE);
        } else if (!can_move)
        {
            on_player_pat((board_.get_player() == board::Color::WHITE)
                              ? board::Color::BLACK
                              : board::Color::WHITE);
            on_draw();
            on_game_finished();
        }

        board_.end_turn();
    }

    void ListenerManager::register_board(const board::Chessboard& board)
    {
        board_ = board;
        for (const auto& l : listeners_)
            l->register_board(board);
    }

    void ListenerManager::on_game_finished()
    {
        for (const auto& l : listeners_)
            l->on_game_finished();
    }

    void ListenerManager::on_piece_moved(const board::PieceType piece,
                                         const board::Position& from,
                                         const board::Position& to)
    {
        for (const auto& l : listeners_)
            l->on_piece_moved(piece, from, to);
    }

    void ListenerManager::on_piece_taken(const board::PieceType piece,
                                         const board::Position& at)
    {
        for (const auto& l : listeners_)
            l->on_piece_taken(piece, at);
    }

    void ListenerManager::on_piece_promoted(const board::PieceType piece,
                                            const board::Position& at)
    {
        for (const auto& l : listeners_)
            l->on_piece_promoted(piece, at);
    }

    void ListenerManager::on_kingside_castling(const board::Color color)
    {
        for (const auto& l : listeners_)
            l->on_kingside_castling(color);
    }

    void ListenerManager::on_queenside_castling(const board::Color color)
    {
        for (const auto& l : listeners_)
            l->on_queenside_castling(color);
    }

    void ListenerManager::on_player_check(const board::Color color)
    {
        for (const auto& l : listeners_)
            l->on_player_check(color);
    }

    void ListenerManager::on_player_mat(const board::Color color)
    {
        for (const auto& l : listeners_)
            l->on_player_mat(color);
    }

    void ListenerManager::on_player_pat(const board::Color color)
    {
        for (const auto& l : listeners_)
            l->on_player_pat(color);
    }

    void ListenerManager::on_player_disqualified(const board::Color color)
    {
        for (const auto& l : listeners_)
            l->on_player_disqualified(color);
    }

    void ListenerManager::on_draw()
    {
        for (const auto& l : listeners_)
            l->on_draw();
    }
} // namespace listener
