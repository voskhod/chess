#include <boost/program_options.hpp>
#include <iostream>

#include "listener-manager.hh"

namespace po = boost::program_options;

void init_desc(po::options_description& desc)
{
    desc.add_options()("help,h", "show usage")(
        "pgn", po::value<std::string>()->default_value(""),
        "path to the PGN game file")(
        "listeners,l", po::value<std::vector<std::string>>()->multitoken(),
        "list of paths to listener plugins")(
        "perft", po::value<std::string>()->default_value(""),
        "path to a perft file");
}

int main(int argc, const char* argv[])
{
    po::options_description desc{"Allowed options"};
    init_desc(desc);

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, desc), vm);

    if (vm.count("help"))
    {
        std::cout << desc << '\n';
        return 0;
    }

    std::vector<std::string> listeners_paths;
    if (vm.count("listeners"))
        listeners_paths = vm["listeners"].as<std::vector<std::string>>();

    std::string perft_path = vm["perft"].as<std::string>();
    std::string pgn_path = vm["pgn"].as<std::string>();

    listener::ListenerManager lm(listeners_paths);

    /* Run from the perft, pgn file or AI */
    if (perft_path != "" && pgn_path != "")
        std::cerr << "Cannot have perft and pgn set together";
    else if (perft_path != "")
        lm.run_perft(perft_path);
    else if (pgn_path != "")
        lm.run_pgn(pgn_path);
    else
        lm.play_ai();

    lm.close_listeners();

    return 0;
}
