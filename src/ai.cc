#include "ai.hh"

#include <algorithm>
#include <limits>
#include <random>

namespace ai
{
    using namespace board;
    static unsigned piece_count = 32;

    Move find_capture_of_king(const Chessboard& board, Rank r)
    {
        auto moves = board.generate_legal_moves();
        auto res =
            std::find_if(moves.begin(), moves.end(), [&r](const Move& a) {
                return a.to() == Position(File::F, r);
            });

        return (res != moves.end()) ? *res : moves.at(0);
    }

    Move search_with_opening(const Chessboard& board)
    {
        if (board.is_check(board.get_player() == Color::WHITE ? Color::BLACK
                                                              : Color::WHITE))
            return search(board);

        switch (board.turn_count())
        {
        /* White turn */
        case 1:
            return Move(PieceType::PAWN, Position(File::E, Rank::TWO),
                        Position(File::E, Rank::FOUR));

        /* Black turn */
        case 2:
            return Move(PieceType::PAWN, Position(File::E, Rank::SEVEN),
                        Position(File::E, Rank::FIVE));

        /* White turn */
        case 3:
            return Move(PieceType::QUEEN, Position(File::D, Rank::ONE),
                        Position(File::H, Rank::FIVE));

        /* Black turn */
        case 4:
            return Move(PieceType::QUEEN, Position(File::D, Rank::EIGHT),
                        Position(File::H, Rank::FOUR));

        /* White turn */
        case 5:
            return Move(PieceType::BISHOP, Position(File::F, Rank::ONE),
                        Position(File::C, Rank::FOUR));

        /* Black turn */
        case 6:
            return Move(PieceType::BISHOP, Position(File::F, Rank::EIGHT),
                        Position(File::C, Rank::FIVE));

        /* White turn */
        case 7:
            return find_capture_of_king(board, Rank::SEVEN);

        /* Black turn */
        case 8:
            return find_capture_of_king(board, Rank::TWO);

        /* Classic negamax */
        default:
            return search(board);
        }
    }

    Move search(const board::Chessboard& board)
    {
        Move bestmove(PieceType::PAWN, Position(File::A, Rank::ONE),
                      Position(File::A, Rank::ONE));

        int alpha = min_int;
        auto moves = board.generate_legal_moves();
        std::random_shuffle(moves.begin(), moves.end());

        for (const auto& m : moves)
        {
            Chessboard tmp(board);
            tmp.do_move(m);
            tmp.end_turn();

            if (tmp.is_check(tmp.get_player()))
                continue;

            /* The less piece stay on the board, more we go deeper. The depth
             * must always be odd
             */
            int depth = (((128 / piece_count) >> 1) << 1) + 1;
            int score = negamax(tmp, depth, min_int, max_int);

            if (score > alpha)
            {
                bestmove = m;
                alpha = score;
            }
        }

        return bestmove;
    }

    int negamax(const Chessboard& board, int depth, int alpha, int beta)
    {
        if (depth == 0)
            return -evaluate(board);

        for (const auto& m : board.generate_legal_moves())
        {
            Chessboard tmp(board);
            tmp.do_move(m);
            tmp.end_turn();

            if (tmp.is_check(tmp.get_player()))
                continue;

            int score = -negamax(tmp, depth - 1, -beta, -alpha);

            if (score >= beta)
                return beta;

            if (score > alpha)
                alpha = score;
        }
        return alpha;
    }

    int piece_value(const PieceType& p)
    {
        switch (p)
        {
        case PieceType::KING:
            return 200;
        case PieceType::QUEEN:
            return 9;
        case PieceType::ROOK:
            return 5;
        case PieceType::BISHOP:
            return 3;
        case PieceType::KNIGHT:
            return 3;
        case PieceType::PAWN:
            return 1;
        }
        return 0;
    }

    int evaluate(const Chessboard& board)
    {
        if (!board.can_move())
            return min_int;

        int score = 0;
        piece_count = 0;

        for (unsigned i = 0; i < 8; i++)
        {
            for (unsigned j = 0; j < 8; j++)
            {
                piece_count++;
                auto p = board[Position(board::File(i), board::Rank(j))];
                if (p)
                {
                    if (p->second == board.get_player())
                        score += piece_value(p->first);
                    else
                        score -= piece_value(p->first);
                }
            }
        }

        return score;
    }
} // namespace ai
