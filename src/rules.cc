#include "rules.hh"

#include <iostream>

#include "piece-type.hh"
#include "utype.hh"

namespace board
{
    /* Return true if the dest if occupied, WHITE or BLACK */
    static bool append_with_capture(std::vector<Move>& res,
                                    const Chessboard& board,
                                    const PieceType piece, const Position from,
                                    const Position to)
    {
        auto dest_piece = board[to];

        if (dest_piece)
        {
            /* Capture */
            if (dest_piece->second != board.get_color_at(from))
                res.push_back(Move(piece, from, to, dest_piece->first));

            /* No else because no move possible */
            return true;
        } else
            res.push_back(Move(piece, from, to));

        return false;
    }

    static inline void append_with_check_bound(std::vector<Move>& res,
                                               const Chessboard& board,
                                               const PieceType piece,
                                               const Position from,
                                               const Position to)
    {
        unsigned newFile = utils::utype(to.file_get());
        unsigned newRank = utils::utype(to.rank_get());

        if (newFile < 8 && newRank < 8)
            append_with_capture(res, board, piece, from, to);
    }

    void inline append_pawn(std::vector<Move>& res, const Position& from,
                            const Position& to)
    {
        if (to.rank_get() == Rank::ONE || to.rank_get() == Rank::EIGHT)
        {
            res.push_back(Move(PieceType::PAWN, from, to)
                              .set_promotion(PieceType::QUEEN));

            res.push_back(
                Move(PieceType::PAWN, from, to).set_promotion(PieceType::ROOK));

            res.push_back(Move(PieceType::PAWN, from, to)
                              .set_promotion(PieceType::BISHOP));

            res.push_back(Move(PieceType::PAWN, from, to)
                              .set_promotion(PieceType::KNIGHT));
        } else
            res.push_back(Move(PieceType::PAWN, from, to));
    }

    void inline append_pawn_capture(std::vector<Move>& res,
                                    const Position& from, const Position& to,
                                    const PieceType& capture)
    {
        if (to.rank_get() == Rank::ONE || to.rank_get() == Rank::EIGHT)
        {
            res.push_back(Move(PieceType::PAWN, from, to, capture)
                              .set_promotion(PieceType::QUEEN));

            res.push_back(Move(PieceType::PAWN, from, to, capture)
                              .set_promotion(PieceType::ROOK));

            res.push_back(Move(PieceType::PAWN, from, to, capture)
                              .set_promotion(PieceType::BISHOP));

            res.push_back(Move(PieceType::PAWN, from, to, capture)
                              .set_promotion(PieceType::KNIGHT));
        } else
            res.push_back(Move(PieceType::PAWN, from, to, capture));
    }

    void generate_pawn_moves(const Chessboard& board, const Position& pos,
                             std::vector<Move>& res)
    {
        int dir = (board.get_color_at(pos) == Color::WHITE) ? 1 : -1;
        int i = utils::utype(pos.rank_get());

        if ((pos.rank_get() == Rank::TWO
             && board.get_color_at(pos) == Color::WHITE)
            || (pos.rank_get() == Rank::SEVEN
                && board.get_color_at(pos) == Color::BLACK))
        {
            auto to = Position(pos.file_get(), Rank(i + 2 * dir));
            if (!board.is_occupied(to)
                && !board.is_occupied(Position(pos.file_get(), Rank(i + dir))))
                res.push_back(
                    Move(PieceType::PAWN, pos, to).set_double_pawn_push());
        }

        // One forward
        if (i + 1 >= 0 && i + 1 < 8)
        {
            auto to = Position(pos.file_get(), Rank(i + dir));
            if (!board.is_occupied(to))
                append_pawn(res, pos, to);
        }

        generate_pawn_attacks(board, pos, res);
    }

    void generate_pawn_attacks(const Chessboard& board, const Position& pos,
                               std::vector<Move>& res)
    {
        int dir = (board.get_color_at(pos) == Color::WHITE) ? 1 : -1;
        int i = utils::utype(pos.rank_get());

        /* Forward right */
        if (pos.file_get() != File::H && pos.rank_get() != Rank::ONE
            && pos.rank_get() != Rank::EIGHT)
        {
            auto forward_right =
                Position(File(utils::utype(pos.file_get()) + 1), Rank(i + dir));
            auto forward_right_piece = board[forward_right];

            /* Simple capture */
            if (forward_right_piece
                && forward_right_piece->second != board.get_color_at(pos))
            {
                append_pawn_capture(res, pos, forward_right,
                                    forward_right_piece->first);
            }

            /* En passant */
            if (board.get_en_passant()
                && forward_right == *board.get_en_passant())
            {
                auto to = Position(forward_right.file_get(), Rank(i + dir));

                res.push_back(Move(PieceType::PAWN, pos, to, PieceType::PAWN)
                                  .set_en_passant());
            }
        }

        /* Forward left piece*/
        if (pos.file_get() != File::A && pos.rank_get() != Rank::ONE
            && pos.rank_get() != Rank::EIGHT)
        {
            auto forward_left =
                Position(File(utils::utype(pos.file_get()) - 1), Rank(i + dir));
            auto forward_left_piece = board[forward_left];

            /* Simple capture */
            if (forward_left_piece
                && forward_left_piece->second != board.get_color_at(pos))
            {
                append_pawn_capture(res, pos, forward_left,
                                    forward_left_piece->first);
            }

            /* En passant */
            if (board.get_en_passant()
                && forward_left == *board.get_en_passant())
            {
                auto to = Position(forward_left.file_get(), Rank(i + dir));
                res.push_back(Move(PieceType::PAWN, pos, to, PieceType::PAWN)
                                  .set_en_passant());
            }
        }
    }

    void generate_king_moves(const Chessboard& board, const Position& pos,
                             std::vector<Move>& res)
    {
        unsigned rank = utils::utype(pos.rank_get());
        unsigned file = utils::utype(pos.file_get());

        PieceType pieceType = PieceType::KING;

        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file), Rank(rank + 1)));
        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file + 1), Rank(rank + 1)));
        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file - 1), Rank(rank + 1)));
        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file + 1), Rank(rank)));
        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file - 1), Rank(rank)));
        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file - 1), Rank(rank - 1)));
        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file), Rank(rank - 1)));
        append_with_check_bound(res, board, pieceType, pos,
                                Position(File(file + 1), Rank(rank - 1)));
    }

    void generate_bishop_moves(const Chessboard& board, const Position& pos,
                               std::vector<Move>& res)
    {
        generate_diag(board, PieceType::BISHOP, pos, res);
    }

    void generate_rook_moves(const Chessboard& board, const Position& pos,
                             std::vector<Move>& res)
    {
        generate_cross(board, PieceType::ROOK, pos, res);
    }

    void generate_queen_moves(const Chessboard& board, const Position& pos,
                              std::vector<Move>& res)
    {
        generate_diag(board, PieceType::QUEEN, pos, res);
        generate_cross(board, PieceType::QUEEN, pos, res);
    }

    void generate_knight_moves(const Chessboard& board, const Position& pos,
                               std::vector<Move>& res)
    {
        unsigned file = utils::utype(pos.file_get());
        unsigned rank = utils::utype(pos.rank_get());

        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file + 1), Rank(rank + 2)));
        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file + 2), Rank(rank + 1)));
        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file + 2), Rank(rank - 1)));
        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file + 1), Rank(rank - 2)));
        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file - 1), Rank(rank - 2)));
        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file - 2), Rank(rank - 1)));
        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file - 2), Rank(rank + 1)));
        append_with_check_bound(res, board, PieceType::KNIGHT, pos,
                                Position(File(file - 1), Rank(rank + 2)));
    }

    void generate_diag(const Chessboard& board, const PieceType& piece,
                       const Position& pos, std::vector<Move>& res)
    {
        unsigned rank = utils::utype(pos.rank_get());
        unsigned file = utils::utype(pos.file_get());

        /* North-East */
        for (unsigned i = 1; file + i < 8 && rank + i < 8; ++i)
        {
            Position newPos(File(file + i), Rank(rank + i));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }

        /* North-West */
        for (unsigned i = 1; file - i < 8 && rank + i < 8; ++i)
        {
            Position newPos(File(file - i), Rank(rank + i));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }

        /* South-East */
        for (unsigned i = 1; file + i < 8 && rank - i < 8; ++i)
        {
            Position newPos(File(file + i), Rank(rank - i));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }

        /* South-West */
        for (unsigned i = 1; file - i < 8 && rank - i < 8; ++i)
        {
            Position newPos(File(file - i), Rank(rank - i));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }
    }

    void generate_cross(const Chessboard& board, const PieceType& piece,
                        const Position& pos, std::vector<Move>& res)
    {
        unsigned rank = utils::utype(pos.rank_get());
        unsigned file = utils::utype(pos.file_get());

        /* East */
        for (unsigned i = file + 1; i < 8; ++i)
        {
            Position newPos(File(static_cast<int>(i)), Rank(rank));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }

        /* West */
        for (unsigned i = file - 1; i < 8; --i)
        {
            Position newPos(File(static_cast<int>(i)), Rank(rank));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }

        /* North */
        for (unsigned i = rank + 1; i < 8; ++i)
        {
            Position newPos(File(file), Rank(static_cast<int>(i)));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }

        /* South */
        for (unsigned i = rank - 1; i < 8; --i)
        {
            Position newPos(File(file), Rank(static_cast<int>(i)));

            if (append_with_capture(res, board, piece, pos, newPos))
                break;
        }
    }

    char update_castling(const u64 king_board, const u64 rook_board,
                         char castling)
    {
        if (!(castling & white_big_castling_possible)
            || !(rook_board & wrl_initial_pos)
            || !(king_board & wk_initial_pos))
            castling &= ~white_big_castling_possible;

        if (!(castling & white_small_castling_possible)
            || !(rook_board & wrr_initial_pos)
            || !(king_board & wk_initial_pos))
            castling &= ~white_small_castling_possible;

        if (!(castling & black_small_castling_possible)
            || !(rook_board & brr_initial_pos)
            || !(king_board & bk_initial_pos))
            castling &= ~black_small_castling_possible;

        if (!(castling & black_big_castling_possible)
            || !(rook_board & brl_initial_pos)
            || !(king_board & bk_initial_pos))
            castling &= ~black_big_castling_possible;

        return castling;
    }

    void generate_king_castling(const u64 occupency, const Color c,
                                char castling, std::vector<Move>& res)
    {
        if (!castling)
            return;

        if (c == Color::WHITE)
        {
            /* Check if the Rook is at A8, King is at E8 and if the space
             * between them is free*/
            if ((castling & white_big_castling_possible)
                && !(occupency & wbc_free))
                res.push_back(Move(PieceType::KING,
                                   Position(File::E, Rank::ONE),
                                   Position(File::C, Rank::ONE))
                                  .set_queenside_castling());

            /* Rook at H8, King at E8 */
            if ((castling & white_small_castling_possible)
                && !(occupency & wsc_free))
                res.push_back(Move(PieceType::KING,
                                   Position(File::E, Rank::ONE),
                                   Position(File::G, Rank::ONE))
                                  .set_kingside_castling());
        } else
        {
            /* Check if the Rook is at A8, King is at E8 and if the space
             * between them is free*/
            if ((castling & black_small_castling_possible)
                && !(occupency & bsc_free))
                res.push_back(Move(PieceType::KING,
                                   Position(File::E, Rank::EIGHT),
                                   Position(File::G, Rank::EIGHT))
                                  .set_kingside_castling());

            /* Rook at H8, King at E8 */
            if ((castling & black_big_castling_possible)
                && !(occupency & bbc_free))
                res.push_back(Move(PieceType::KING,
                                   Position(File::E, Rank::EIGHT),
                                   Position(File::C, Rank::EIGHT))
                                  .set_queenside_castling());
        }
    }
} // namespace board
