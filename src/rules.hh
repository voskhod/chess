#pragma once

#include <vector>

#include "chessboard.hh"
#include "color.hh"

namespace board
{
    /*
        void generate_pawn_moves(u64 pawns,
                                    u64 occupency,
                                    Color player,
                                    std::vector<Move>& res);
    */

    void generate_pawn_moves(const Chessboard& board, const Position& pos,
                             std::vector<Move>& res);

    void generate_pawn_attacks(const Chessboard& board, const Position& pos,
                               std::vector<Move>& res);

    void generate_king_moves(const Chessboard& board, const Position& pos,
                             std::vector<Move>& res);

    void generate_bishop_moves(const Chessboard& board, const Position& pos,
                               std::vector<Move>& res);

    void generate_rook_moves(const Chessboard& board, const Position& pos,
                             std::vector<Move>& res);

    void generate_queen_moves(const Chessboard& board, const Position& pos,
                              std::vector<Move>& res);

    void generate_knight_moves(const Chessboard& board, const Position& pos,
                               std::vector<Move>& res);

    void generate_moves(const Chessboard& board, const Position& pos,
                        std::vector<Move>& res);

    void generate_king_castling(const u64 occupency, const Color c,
                                char castling, std::vector<Move>& res);

    using move_generator_f = decltype(&generate_queen_moves);

    char update_castling(const u64 king_board, const u64 rook_board,
                         char castling);

    void generate_diag(const Chessboard& board, const PieceType& piece,
                       const Position& pos, std::vector<Move>& res);

    void generate_cross(const Chessboard& board, const PieceType& piece,
                        const Position& pos, std::vector<Move>& res);

    void test_rule();

    static constexpr u64 white_small_castling_possible = 1;
    static constexpr u64 white_big_castling_possible = 2;
    static constexpr u64 black_small_castling_possible = 4;
    static constexpr u64 black_big_castling_possible = 8;

    /* Black king initial pos */
    static constexpr u64 bk_initial_pos =
        Chessboard::get_mask(File::E, Rank::EIGHT);
    /* Black right rook initial pos */
    static constexpr u64 brl_initial_pos =
        Chessboard::get_mask(File::A, Rank::EIGHT);
    /* Black left rook initial pos */
    static constexpr u64 brr_initial_pos =
        Chessboard::get_mask(File::H, Rank::EIGHT);

    /* Mask to check if space if free for big castling */
    static constexpr u64 bbc_free = Chessboard::get_mask(File::B, Rank::EIGHT)
        | Chessboard::get_mask(File::C, Rank::EIGHT)
        | Chessboard::get_mask(File::D, Rank::EIGHT);

    /* Mask to check if space if free for small castling */
    static constexpr u64 bsc_free = Chessboard::get_mask(File::F, Rank::EIGHT)
        | Chessboard::get_mask(File::G, Rank::EIGHT);

    /* White king initial pos */
    static constexpr u64 wk_initial_pos =
        Chessboard::get_mask(File::E, Rank::ONE);
    /* White right rook initial pos */
    static constexpr u64 wrl_initial_pos =
        Chessboard::get_mask(File::A, Rank::ONE);
    /* White left rook initial pos */
    static constexpr u64 wrr_initial_pos =
        Chessboard::get_mask(File::H, Rank::ONE);

    /* White to check if space if free for big castling */
    static constexpr u64 wbc_free = Chessboard::get_mask(File::B, Rank::ONE)
        | Chessboard::get_mask(File::C, Rank::ONE)
        | Chessboard::get_mask(File::D, Rank::ONE);

    /* White to check if space if free for small castling */
    static constexpr u64 wsc_free = Chessboard::get_mask(File::F, Rank::ONE)
        | Chessboard::get_mask(File::G, Rank::ONE);
} // namespace board
