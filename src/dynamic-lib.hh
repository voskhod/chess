#pragma once
#include "listener.hh"

namespace listener
{
    class DynamicLib
    {
    public:
        DynamicLib(const std::string& path);
        DynamicLib(const DynamicLib&) = delete;
        DynamicLib(const DynamicLib&&) = delete;
        DynamicLib& operator=(DynamicLib) = delete;
        DynamicLib& operator=(const DynamicLib&) = delete;

        void close();
        Listener& operator*() const;
        Listener* operator->() const;

    private:
        void* handler_;
        Listener* listener_;
    };
} // namespace listener
