#include <limits>

#include "chessboard.hh"
#include "move.hh"

namespace ai
{
    using namespace board;

    Move search_with_opening(const Chessboard& board);
    Move search(const Chessboard& board);

    int evaluate(const Chessboard& board);

    int negamax(const Chessboard& board, int depth, int alpha, int beta);
    int piece_value(const PieceType& p);

    constexpr int min_int = std::numeric_limits<int>::min() + 1;
    constexpr int max_int = std::numeric_limits<int>::max();

} // namespace ai
