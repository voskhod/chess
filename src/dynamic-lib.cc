#include "dynamic-lib.hh"

#include <dlfcn.h>

namespace listener
{
    DynamicLib::DynamicLib(const std::string& path)
    {
        handler_ = dlopen(path.c_str(), RTLD_NOW);
        if (handler_ == nullptr)
            throw std::invalid_argument(dlerror());

        void* symbol = dlsym(handler_, "listener_create");
        if (symbol == nullptr)
            throw std::invalid_argument(dlerror());

        listener_ = reinterpret_cast<Listener* (*)()>(symbol)();
    }

    void DynamicLib::close()
    {
        delete listener_;
        listener_ = nullptr;
        dlclose(handler_);
    }

    Listener& DynamicLib::operator*() const
    {
        return *listener_;
    }

    Listener* DynamicLib::operator->() const
    {
        return listener_;
    }
} // namespace listener
