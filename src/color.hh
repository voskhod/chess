#pragma once

#include <iostream>

namespace board
{
    /* The Color enum represent a side. */
    enum class Color : bool
    {
        WHITE = false,
        BLACK = true
    };

    inline std::ostream& operator<<(std::ostream& os, const Color& color)
    {
        if (color == Color::BLACK)
            return os << "black";
        else
            return os << "white";
    }
} // namespace board
