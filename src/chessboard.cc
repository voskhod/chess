#include "chessboard.hh"

#include <algorithm>
#include <functional>
#include <iostream>
#include <optional>

#include "fen_object.hh"
#include "rules.hh"
#include "utype.hh"

namespace board
{
    Chessboard::Chessboard()
    {
        en_passant_ = std::nullopt;
        for (unsigned i = 0; i < 8; ++i)
        {
            set_piece({File(i), Rank::TWO}, {PieceType::PAWN, Color::WHITE});
            set_piece({File(i), Rank::SEVEN}, {PieceType::PAWN, Color::BLACK});
        }

        set_piece({File::H, Rank::EIGHT}, {PieceType::ROOK, Color::BLACK});
        set_piece({File::A, Rank::EIGHT}, {PieceType::ROOK, Color::BLACK});
        set_piece({File::H, Rank::ONE}, {PieceType::ROOK, Color::WHITE});
        set_piece({File::A, Rank::ONE}, {PieceType::ROOK, Color::WHITE});

        set_piece({File::G, Rank::EIGHT}, {PieceType::KNIGHT, Color::BLACK});
        set_piece({File::B, Rank::EIGHT}, {PieceType::KNIGHT, Color::BLACK});
        set_piece({File::G, Rank::ONE}, {PieceType::KNIGHT, Color::WHITE});
        set_piece({File::B, Rank::ONE}, {PieceType::KNIGHT, Color::WHITE});

        set_piece({File::C, Rank::EIGHT}, {PieceType::BISHOP, Color::BLACK});
        set_piece({File::F, Rank::EIGHT}, {PieceType::BISHOP, Color::BLACK});
        set_piece({File::C, Rank::ONE}, {PieceType::BISHOP, Color::WHITE});
        set_piece({File::F, Rank::ONE}, {PieceType::BISHOP, Color::WHITE});

        set_piece({File::D, Rank::EIGHT}, {PieceType::QUEEN, Color::BLACK});
        set_piece({File::D, Rank::ONE}, {PieceType::QUEEN, Color::WHITE});

        set_piece({File::E, Rank::EIGHT}, {PieceType::KING, Color::BLACK});
        set_piece({File::E, Rank::ONE}, {PieceType::KING, Color::WHITE});
    }

    Chessboard::Chessboard(const FenObject& fen)
    {
        opt_piece_t current;
        for (unsigned i = 0; i < 8; i++)
        {
            for (unsigned j = 0; j < 8; j++)
            {
                auto piece = fen[Position(File(i), Rank(j))];
                if (piece)
                    set_piece(Position(File(i), Rank(7 - j)), *piece);
            }
        }

        white_turn_ = (fen.side_to_move_get() == Color::WHITE);
        en_passant_ = fen.en_passant_target_get();

        castling_ = 0;
        for (const auto& can_castle : fen.castling_get())
        {
            switch (can_castle)
            {
            case 'K':
                castling_ |= white_small_castling_possible;
                break;

            case 'Q':
                castling_ |= white_big_castling_possible;
                break;

            case 'k':
                castling_ |= black_small_castling_possible;
                break;

            case 'q':
                castling_ |= black_big_castling_possible;
                break;
            }
        }
        castling_ = update_castling(king_, rook_, castling_);
    }

    std::ostream& operator<<(std::ostream& os, const Chessboard& board)
    {
        os << "  ";
        for (unsigned i = 1; i <= 8; ++i)
            os << ' ' << i << ' ';
        os << std::endl << "-------------------------" << std::endl;
        ;

        for (unsigned i = 0; i < 8; ++i)
        {
            os << static_cast<char>('A' + i) << "|";
            for (unsigned j = 0; j < 8; ++j)
            {
                os << ' ';
                auto r = board.get_piece({File(i), Rank(j)});

                if (r)
                {
                    if (r->second == Color::WHITE)
                        os << (*r).first << ' ';
                    else
                        os << "\033[31m" << (*r).first << "\033[39m" << ' ';
                } else
                    os << "  ";
            }
            os << std::endl;
        }

        return os;
    }

    void Chessboard::do_move(const Move& move)
    {
        if (move.kingside_castling())
            move_piece(Move(PieceType::ROOK,
                            Position(File::H, move.to().rank_get()),
                            Position(File::F, move.to().rank_get())),
                       get_player());

        if (move.queenside_castling())
            move_piece(Move(PieceType::ROOK,
                            Position(File::A, move.to().rank_get()),
                            Position(File::D, move.to().rank_get())),
                       get_player());

        if (move.en_passant())
        {
            Position to_delete(move.to().file_get(), move.from().rank_get());
            delete_piece(to_delete);
        } else if (move.capture())
            delete_piece(move.to());

        if (move.double_pawn_push())
        {
            int dir = (white_turn_) ? -1 : 1;
            Rank r = Rank(utils::utype(move.to().rank_get()) + dir);
            en_passant_ = {Position(move.to().file_get(), r)};
        } else
            en_passant_ = std::nullopt;

        move_piece(move, get_player());

        if (move.promotion())
        {
            delete_piece(move.to());
            set_piece(move.to(), {*move.promotion(), get_player()});
        }

        castling_ = update_castling(king_, rook_, castling_);
        turn_++;
    }

    Chessboard::opt_piece_t
    Chessboard::get_piece(const Position& position) const
    {
        u64 mask = get_mask(position);

        Color color;
        if (white_ & mask)
            color = Color::WHITE;
        else if (black_ & mask)
            color = Color::BLACK;
        else
            return std::nullopt;

        if (pawn_ & mask)
            return {{PieceType::PAWN, color}};
        else if (rook_ & mask)
            return {{PieceType::ROOK, color}};
        else if (bishop_ & mask)
            return {{PieceType::BISHOP, color}};
        else if (knight_ & mask)
            return {{PieceType::KNIGHT, color}};
        else if (queen_ & mask)
            return {{PieceType::QUEEN, color}};
        else if (king_ & mask)
            return {{PieceType::KING, color}};
        else
            throw std::invalid_argument("chessboard::get : c'est la merde");
    }

    void Chessboard::set_piece(const Position& position,
                               const side_piece_t& piece)
    {
        u64 bitset = get_mask(position);

        if (piece.second == Color::WHITE)
            white_ |= bitset;
        else
            black_ |= bitset;

        switch (piece.first)
        {
        case PieceType::QUEEN: {
            queen_ |= bitset;
            return;
        }
        case PieceType::ROOK: {
            rook_ |= bitset;
            return;
        }
        case PieceType::BISHOP: {
            bishop_ |= bitset;
            return;
        }
        case PieceType::KNIGHT: {
            knight_ |= bitset;
            return;
        }
        case PieceType::PAWN: {
            pawn_ |= bitset;
            return;
        }
        case PieceType::KING: {
            king_ |= bitset;
            return;
        }
        }
    }

    void Chessboard::delete_piece(const Position& position)
    {
        u64 rank = utils::utype(position.rank_get());
        u64 file = utils::utype(position.file_get());
        u64 bitset = 1ULL << (rank + file * 8ULL);

        pawn_ = pawn_ & ~bitset;
        knight_ = knight_ & ~bitset;
        bishop_ = bishop_ & ~bitset;
        rook_ = rook_ & ~bitset;
        queen_ = queen_ & ~bitset;
        king_ = king_ & ~bitset;

        white_ = white_ & ~bitset;
        black_ = black_ & ~bitset;
    }

    void Chessboard::generate_moves(std::vector<Move>& res, const Position& pos,
                                    const Color& color) const
    {
        /* *this, pos, res */
        static const move_generator_f move_gen[] = {
            generate_queen_moves,  generate_rook_moves, generate_bishop_moves,
            generate_knight_moves, generate_pawn_moves, generate_king_moves};

        auto piece = get_piece(pos);

        if (piece)
        {
            if (piece->second != color)
                return;

            int i = utils::utype(piece->first);
            move_gen[i](*this, pos, res);
        }
    }

    std::vector<Move>
    Chessboard::generate_pseudolegal_moves(const Color& c) const
    {
        std::vector<Move> moves;
        moves.reserve(128);

        for (int i = 0; i < 8; ++i)
        {
            for (int j = 0; j < 8; ++j)
                generate_moves(moves, Position(File(i), Rank(j)), c);
        }

        return moves;
    }

    std::vector<Move> Chessboard::generate_legal_moves() const
    {
        auto moves = generate_pseudolegal_moves(get_player());

        auto adv = (white_turn_) ? Color::BLACK : Color::WHITE;
        if (!is_check(adv))
            generate_king_castling(black_ | white_, get_player(), castling_,
                                   moves);

        return moves;
    }

    std::optional<Move> Chessboard::is_legal_move(const Move& move) const
    {
        std::vector<Move> legal_moves;
        legal_moves.reserve(32);

        generate_moves(legal_moves, move.from(), get_player());
        if (move.get_piece() == PieceType::KING)
        {
            generate_king_castling(black_ | white_, get_player(), castling_,
                                   legal_moves);
        }

        auto it = std::find(legal_moves.begin(), legal_moves.end(), move);

        return (it != legal_moves.end()) ? std::optional(*it) : std::nullopt;
    }

    bool Chessboard::is_check(const Color& c) const
    {
        for (const auto& m : generate_pseudolegal_moves(c))
        {
            if (m.capture())
            {
                auto p = *get_piece(m.to());
                if (p.first == PieceType::KING)
                    return true;
            }
        }

        return false;
    }

    bool Chessboard::is_unsafe(const Move& move) const
    {
        auto adv = (white_turn_) ? Color::BLACK : Color::WHITE;
        Chessboard tmp(*this);

        if (move.capture())
            tmp.delete_piece(move.to());

        tmp.move_piece(move, adv);

        return tmp.is_check(get_player());
    }

    bool Chessboard::can_move() const
    {
        auto adv = (white_turn_) ? Color::BLACK : Color::WHITE;

        for (const auto& m : generate_pseudolegal_moves(adv))
        {
            if (!is_unsafe(m))
                return true;
        }

        return false;
    }
} // namespace board
