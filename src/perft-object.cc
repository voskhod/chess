#include "perft-object.hh"

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "chessboard.hh"

namespace board
{
    PerftObject PerftObject::parse_from_file(const std::string& path)
    {
        std::ifstream in(path);
        std::string line;

        std::getline(in, line);
        return parse(line);
    }

    PerftObject PerftObject::parse(std::string str)
    {
        std::vector<std::string> vect;
        boost::split(vect, str, boost::is_any_of(" "));
        std::string depth_str = vect[vect.size() - 1];
        vect.pop_back();
        FenObject fen_obj = FenObject::parse(vect);
        int depth = std::stoi(depth_str);
        return PerftObject(fen_obj, depth);
    }

    PerftObject::u64 evaluate_rec(unsigned depth, Chessboard& board)
    {
        if (depth == 0)
            return 1;

        u64 nodes = 0;

        for (const Move& move : board.generate_legal_moves())
        {
            Chessboard copy(board);

            copy.do_move(move);
            copy.end_turn();

            if (!copy.is_check(copy.get_player()))
            {
                /*
                    print(move);
                    std::cerr << "\n";
                    std::cerr << copy << std::endl;
                    std::cerr << "\n";
                */
                nodes += evaluate_rec(depth - 1, copy);
            }
        }

        return nodes;
    }

    PerftObject::u64 PerftObject::evaluate(unsigned depth)
    {
        // Step 1 Generate the chessboard
        Chessboard board(fen_);
        // std::cerr << board << std::endl;
        return evaluate_rec(depth, board);
    }
} // namespace board
