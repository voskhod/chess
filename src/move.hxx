#include "utype.hh"

namespace board
{
    inline Move::Move(const PieceType type, const Position from,
                      const Position to)
    {
        /* Init the optional to nullopt and set the type */
        move_ = default_value | utils::utype(type);

        /* From */
        move_ |= utils::utype(from.file_get()) << 3;
        move_ |= utils::utype(from.rank_get()) << 6;

        /* To */
        move_ |= utils::utype(to.file_get()) << 9;
        move_ |= utils::utype(to.rank_get()) << 12;
    }

    inline Move::Move(const PieceType type, const Position from,
                      const Position to, PieceType capture)
    {
        /* Init the optional to nullopt and set the type */
        move_ = default_value | utils::utype(type);

        /* From */
        move_ |= utils::utype(from.file_get()) << 3;
        move_ |= utils::utype(from.rank_get()) << 6;

        /* To */
        move_ |= utils::utype(to.file_get()) << 9;
        move_ |= utils::utype(to.rank_get()) << 12;

        set_capture(capture);
    }

    inline PieceType Move::get_piece() const noexcept
    {
        return PieceType(move_ & 0x7);
    }

    inline Position Move::from() const noexcept
    {
        return Position(File((move_ >> 3) & 0x7), Rank((move_ >> 6) & 0x7));
    }

    inline Position Move::to() const noexcept
    {
        return Position(File((move_ >> 9) & 0x7), Rank((move_ >> 12) & 0x7));
    }

    inline bool Move::operator==(const Move& rhs) const noexcept
    {
        /* Only compare from and to */

        return rhs.to() == to() && rhs.from() == from()
            && rhs.get_piece() == get_piece()
            && ((bool)rhs.capture() == (bool)capture());
    }

    inline int Move::value() const noexcept
    {
        int val = 0;

        if (promotion())
            val += utils::utype(*promotion());

        if (kingside_castling() || queenside_castling())
            val += 5;

        if (capture())
            val += utils::utype(*capture()) - utils::utype(get_piece());

        if (get_piece() == PieceType::KING)
        {
            if (to().rank_get() != Rank::ONE && to().rank_get() != Rank::EIGHT)
                val -= 5;
        }

        return val;
    }

    inline bool Move::operator<(const Move& rhs) const noexcept
    {
        return value() > rhs.value();
    }

    inline std::optional<PieceType> Move::promotion() const noexcept
    {
        auto i = (move_ >> 16) & 0x7;
        if (i == 0x7)
            return std::nullopt;
        return {PieceType(i)};
    }

    inline std::optional<PieceType> Move::capture() const noexcept
    {
        auto i = (move_ >> 19) & 0x7;
        if (i == 0x7)
            return std::nullopt;
        return {PieceType(i)};
    }

    inline bool Move::double_pawn_push() const noexcept
    {
        return move_ & mask_double_pawn_;
    }

    inline bool Move::kingside_castling() const noexcept
    {
        return move_ & mask_kingside_castling_;
    }

    inline bool Move::queenside_castling() const noexcept
    {
        return move_ & mask_queenside_castling_;
    }

    inline bool Move::en_passant() const noexcept
    {
        return move_ & mask_en_passant_;
    }

    inline Move Move::set_promotion(PieceType promotion) noexcept
    {
        move_ =
            (move_ & ~mask_promotion) | (utils::utype(promotion) & 0x7) << 16;
        return *this;
    }

    inline Move Move::set_capture(PieceType capture) noexcept
    {
        move_ = (move_ & ~mask_capture) | (utils::utype(capture) & 0x7) << 16;
        return *this;
    }

    inline Move Move::set_double_pawn_push() noexcept
    {
        move_ |= mask_double_pawn_;
        return *this;
    }

    inline Move Move::set_kingside_castling() noexcept
    {
        move_ |= mask_kingside_castling_;
        return *this;
    }

    inline Move Move::set_queenside_castling() noexcept
    {
        move_ |= mask_queenside_castling_;
        return *this;
    }

    inline Move Move::set_en_passant() noexcept
    {
        move_ |= mask_en_passant_;
        return *this;
    }

    inline std::ostream& operator<<(std::ostream& os, const Move& move)
    {
        os << move.from() << move.to();
        if (move.promotion())
            os << *move.promotion();
        return os;
    }

    void print(const Move& move)
    {
        std::cerr << "Move " << move.get_piece() << " from " << move.from()
                  << " to " << move.to();

        if (move.promotion())
            std::cerr << " with promotion " << *move.promotion();

        if (move.capture())
            std::cerr << " has capture a " << *move.capture();

        if (move.kingside_castling())
            std::cerr << " kingside_castling ";

        if (move.queenside_castling())
            std::cerr << " queenside_castling ";

        if (move.en_passant())
            std::cerr << " en_passant ";
    }
} // namespace board
