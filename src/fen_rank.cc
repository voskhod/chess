#include "fen_rank.hh"

#include <ctype.h>
#include <string>
#include <vector>

#include "color.hh"
#include "piece-type.hh"
#include "position.hh"
#include "utype.hh"

namespace board
{
    FenRank::FenRank(std::string input)
    {
        unsigned index = 0;
        Color current_color;
        for (auto c = input.begin(); c != input.end() && index < 8; c++)
        {
            if (isdigit(*c))
            {
                index += (*c) - '0';
                continue;
            }
            if (islower(*c))
            {
                current_color = Color::BLACK;
            } else
            {
                current_color = Color::WHITE;
            }

            pieces_[index] = {char_to_piece(toupper(*c)), current_color};
            index++;
        }
    }

    FenRank::opt_piece_t FenRank::operator[](File file) const
    {
        return pieces_[utils::utype(file)];
    }

} // namespace board
