#pragma once

#include <string>
#include <vector>

#include "color.hh"
#include "fen_object.hh"
#include "piece-type.hh"
#include "position.hh"

namespace board
{
    class PerftObject
    {
    public:
        using u64 = unsigned long long;

        ~PerftObject() = default;

        static PerftObject parse(std::string str);
        static PerftObject parse_from_file(const std::string& str);

        u64 evaluate(unsigned depth);

        inline FenObject fen_get()
        {
            return fen_;
        }
        inline int depth_get()
        {
            return depth_;
        }

    private:
        PerftObject(FenObject fen, int depth)
            : fen_(fen)
            , depth_(depth)
        {}

        FenObject fen_;
        int depth_;
    };
} // namespace board
