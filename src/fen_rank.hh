#pragma once

#include <string>
#include <vector>

#include "color.hh"
#include "piece-type.hh"
#include "position.hh"

namespace board
{
    class FenRank
    {
    public:
        using side_piece_t = std::pair<PieceType, Color>;
        using opt_piece_t = std::optional<side_piece_t>;

        FenRank() = default;
        FenRank(std::string input);
        ~FenRank() = default;

        opt_piece_t operator[](File file) const;

    private:
        opt_piece_t pieces_[8];
    };
} // namespace board
