#pragma once

#include <optional>
#include <string>
#include <vector>

#include "color.hh"
#include "fen_rank.hh"
#include "piece-type.hh"
#include "position.hh"

namespace board
{
    class FenObject
    {
    public:
        using side_piece_t = std::pair<PieceType, Color>;
        using opt_piece_t = std::optional<side_piece_t>;

        static FenObject parse(std::vector<std::string> vect);

        ~FenObject() = default;

        inline opt_piece_t operator[](board::Position pos) const
        {
            return ranks_[utils::utype(pos.rank_get())][pos.file_get()];
        }

        inline Color side_to_move_get() const
        {
            return side_to_move_;
        }
        inline std::vector<char> castling_get() const
        {
            return castling_;
        }
        inline std::optional<Position> en_passant_target_get() const
        {
            return en_passant_target_;
        }

    private:
        FenObject(FenRank* ranks, Color side_to_move,
                  std::vector<char> castling,
                  std::optional<Position> en_passant_target);

        FenRank ranks_[8];
        Color side_to_move_;
        std::vector<char> castling_;
        std::optional<Position> en_passant_target_;
    };
} // namespace board
