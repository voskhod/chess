#pragma once

template <class T>
class Singleton
{
public:
    static T* instance()
    {
        return &instance_;
    }

protected:
    static T instance_;

    Singleton()
    {}
    Singleton(const Singleton&) = default;
    Singleton& operator=(const Singleton&) = default;
    T& operator=(const T&)
    {}
};

template <class T>
T Singleton<T>::instance_ = T();
