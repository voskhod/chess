#pragma once

#include <iostream>
#include <optional>
#include <utility>
#include <vector>

#include "chessboard-interface.hh"
#include "color.hh"
#include "fen_object.hh"
#include "move.hh"
#include "position.hh"

namespace board
{
    using u64 = unsigned long long;

    class Chessboard : public ChessboardInterface
    {
    public:
        Chessboard();
        ~Chessboard() = default;
        Chessboard(const FenObject& fen);

        inline opt_piece_t operator[](const Position& position) const;
        inline Color get_player() const;
        inline std::optional<Color>
        get_color_at(const Position& position) const;
        inline bool is_occupied(const Position& position) const;

        inline unsigned turn_count() const
        {
            return turn_;
        }

        inline void end_turn()
        {
            white_turn_ = !white_turn_;
        }

        void do_move(const Move& move);

        inline std::optional<Position> get_en_passant() const
        {
            return en_passant_;
        }
        inline void set_en_passant(const Position& en_passant)
        {
            en_passant_ = en_passant;
        }

        std::vector<Move> generate_legal_moves() const;
        std::optional<Move> is_legal_move(const Move& move) const;

        bool is_check(const Color& c) const;
        bool can_move() const;

        friend std::ostream& operator<<(std::ostream& os,
                                        const Chessboard& board);

        inline static u64 get_mask(const Position& pos);
        static constexpr u64 get_mask(const File& file, const Rank& rank);

        // Test
        friend class ChessboardTest;

        void set_piece(const Position& position, const side_piece_t& piece);
        opt_piece_t get_piece(const Position& position) const;
        bool is_unsafe(const Move& move) const;

    private:
        std::vector<Move> generate_pseudolegal_moves(const Color& c) const;
        void generate_moves(std::vector<Move>& res, const Position& pos,
                            const Color& c) const;

        inline void move_piece(const Move& move, const Color& c);
        void delete_piece(const Position& position);

        u64 pawn_ = 0ULL;
        u64 rook_ = 0ULL;
        u64 bishop_ = 0ULL;
        u64 knight_ = 0ULL;
        u64 queen_ = 0ULL;
        u64 king_ = 0ULL;

        u64 white_ = 0ULL;
        u64 black_ = 0ULL;

        bool white_turn_ = true;

        /**
         * white small castling on bit 1
         * white big castling on bit 2
         * black small castling on bit 3
         * black big castling on bit 4
         */
        char castling_ = 0xF;
        std::optional<Position> en_passant_ = std::nullopt;

        unsigned turn_ = 1;
        unsigned fifty_turn_ = 0;
    };
} // namespace board

#include "chessboard.hxx"
