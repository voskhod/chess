#include "chessboard.hh"

namespace board
{
    inline Chessboard::opt_piece_t
    Chessboard::operator[](const Position& position) const
    {
        return get_piece(position);
    }

    inline Color Chessboard::get_player() const
    {
        return white_turn_ ? Color::WHITE : Color::BLACK;
    }

    inline u64 Chessboard::get_mask(const Position& pos)
    {
        u64 rank = utils::utype(pos.rank_get());
        u64 file = utils::utype(pos.file_get());

        return 1ULL << (rank + file * 8ULL);
    }

    constexpr u64 Chessboard::get_mask(const File& file, const Rank& rank)
    {
        u64 r = utils::utype(rank);
        u64 f = utils::utype(file);

        return 1ULL << (r + f * 8ULL);
    }

    inline void Chessboard::move_piece(const Move& move, const Color& c)
    {
        delete_piece(move.from());
        set_piece(move.to(), {move.get_piece(), c});
    }

    inline std::optional<Color>
    Chessboard::get_color_at(const Position& position) const
    {
        u64 pos = get_mask(position);

        if (pos & white_)
            return {Color::WHITE};
        else if (pos & black_)
            return {Color::BLACK};
        else
            return std::nullopt;
    }

    inline bool Chessboard::is_occupied(const Position& position) const
    {
        return (get_mask(position) & (white_ | black_));
    }
} // namespace board
