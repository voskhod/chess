#pragma once

#include <iostream>

#include "piece-type.hh"
#include "position.hh"

namespace board
{
    class Move
    {
    public:
        inline Move(const PieceType type, const Position from,
                    const Position to);
        inline Move(const PieceType type, const Position from,
                    const Position to, PieceType capture);

        inline PieceType get_piece() const noexcept;
        inline Position from() const noexcept;
        inline Position to() const noexcept;

        inline bool operator==(const Move& rhs) const noexcept;
        inline bool operator<(const Move& rhs) const noexcept;

        inline std::optional<PieceType> promotion() const noexcept;
        inline std::optional<PieceType> capture() const noexcept;

        inline bool double_pawn_push() const noexcept;
        inline bool kingside_castling() const noexcept;
        inline bool queenside_castling() const noexcept;
        inline bool en_passant() const noexcept;

        inline Move set_promotion(PieceType promotion) noexcept;
        inline Move set_capture(PieceType capture) noexcept;
        inline Move set_double_pawn_push() noexcept;
        inline Move set_kingside_castling() noexcept;
        inline Move set_queenside_castling() noexcept;
        inline Move set_en_passant() noexcept;

    private:
        inline int value() const noexcept;

        using u32 = unsigned int;
        /**
         * Bit:     |32..29|28    25|22    19|18   16|15         9|8 3|2     0|
         * Content: | Void |  Flags |    C   |   P   |      To    |    From |
         * Type |
         *
         * Type:            PieceType
         * From:            Postion (file is at bit 3 to 5 and rank 6 to 8)
         * To:              Postion (file is at bit 3 to 5 and rank 6 to 8)
         * P (promotion):   optional<PieceType>
         * C (capture):     optional<PieceType>
         * Flags:           - double_pawn_push (bit 25),
         *                  - kingside_castling (bit 26),
         *                  - queenside_castling (bit 27),
         *                  - en_passant (bit 28)
         *
         * optional<PieceType> representation:
         *      * if it's an nullopt: 0x7 (all bits are set)
         *      * if valid: the value of the PieceType
         */
        u32 move_;

        /* Set the optional to nullopt */
        static constexpr u32 default_value = 0x3F << 16;

        static constexpr u32 mask_promotion = 0x7 << 16;
        static constexpr u32 mask_capture = 0x7 << 19;

        static constexpr u32 mask_double_pawn_ = 1 << 25;
        static constexpr u32 mask_kingside_castling_ = 1 << 26;
        static constexpr u32 mask_queenside_castling_ = 1 << 27;
        static constexpr u32 mask_en_passant_ = 1 << 28;
    };
    inline void print(const Move& move);
    inline std::ostream& operator<<(std::ostream& os, const Move& move);
} // namespace board

#include "move.hxx"
