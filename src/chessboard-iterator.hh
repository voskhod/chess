#pragma once

#include "chessboard-interface.hh"
#include "chessboard.hh"

namespace board
{
    class ChessboardIt
        : public std::iterator<std::output_iterator_tag,
                               ChessboardInterface::side_piece_t>
    {
    public:
        /** Initialize an iterator of all the pieces of the same type and color
         * as piece.
         *
         * @param board: The board on wich we iterate.
         * @param piece: The piece type and color desire.
         * @param f: The start file.
         * @param r: The start rank.
         */
        ChessboardIt(const Chessboard& board, const PieceType piece,
                     const Color color, unsigned f, unsigned r)
            : board_(board)
            , piece_(piece, color)
            , rank_(r)
            , file_(f)
        {
            while (!is_same() && rank_ < 8 && file_ < 8)
                increment();
        }

        /** Get the position of the current piece.
         *
         * @return: The position of the current piece.
         */
        inline Position operator*() const
        {
            return Position(File(file_), Rank(rank_));
        }

        ChessboardIt& operator++()
        {
            do
            {
                increment();
            } while (!is_same() && rank_ < 8 && file_ < 8);

            return *this;
        }

        inline bool operator!=(const ChessboardIt& rhs) const
        {
            return (rank_ != rhs.rank_ || file_ != rhs.file_);
        }

    private:
        /** Go to the next square.
         */
        void increment()
        {
            file_++;
            if (file_ >= 8)
            {
                file_ = 0;
                rank_++;
            }
        }

        /** Return true if the piece at the current square is the same type as
         * the desire pieces.
         */
        inline bool is_same()
        {
            return board_.get_piece(rank_, file_) != std::nullopt
                && *board_.get_piece(rank_, file_) == piece_;
        }

        const Chessboard& board_;
        const ChessboardInterface::side_piece_t piece_;
        unsigned rank_;
        unsigned file_;
    };

    /** Represent all pieces of a kind
     */
    class ChessboardPieces
    {
    public:
        ChessboardPieces(const Chessboard& board, const PieceType pieceType,
                         const Color color)
            : board_(board)
            , pieceType_(pieceType)
            , color_(color)
        {}

        inline ChessboardIt begin() const
        {
            return ChessboardIt(board_, pieceType_, color_, 0, 0);
        }

        ChessboardIt end() const
        {
            return ChessboardIt(board_, PieceType::PAWN, Color::BLACK, 0, 8);
        }

    private:
        const Chessboard& board_;
        const PieceType pieceType_;
        const Color color_;
    };

} // namespace board
