#include "logger.hh"

#include <iostream>

namespace listener
{
    LISTENER_EXPORT(Logger);

    void
    Logger::register_board(const board::ChessboardInterface& board_interface)
    {}

    void Logger::on_game_finished()
    {
        std::cout << "[CHESS]: Game finished" << std::endl;
    }

    void Logger::on_piece_moved(const board::PieceType piece,
                                const board::Position& from,
                                const board::Position& to)
    {
        std::cout << "[CHESS]: move " << piece << " from " << from << " to "
                  << to << std::endl;
    }

    void Logger::on_piece_taken(const board::PieceType piece,
                                const board::Position& at)
    {
        std::cout << "[CHESS]: " << piece << " taken at " << at << std::endl;
    }

    void Logger::on_piece_promoted(const board::PieceType piece,
                                   const board::Position& at)
    {
        std::cout << "[CHESS]: pawn promoted to " << piece << " at " << at
                  << std::endl;
    }

    void Logger::on_kingside_castling(const board::Color color)
    {
        std::cout << "[CHESS]: king castling for " << color << std::endl;
    }

    void Logger::on_queenside_castling(const board::Color color)
    {
        std::cout << "[CHESS]: queen castling for " << color << std::endl;
    }

    void Logger::on_player_check(const board::Color color)
    {
        std::cout << "[CHESS]: " << color << " is check" << std::endl;
    }

    void Logger::on_player_mat(const board::Color color)
    {
        std::cout << "[CHESS]: " << color << " is check" << std::endl;
    }

    void Logger::on_player_pat(const board::Color color)
    {
        std::cout << "[CHESS]: " << color << " is check" << std::endl;
    }

    void Logger::on_player_disqualified(const board::Color color)
    {
        std::cout << "[CHESS]: " << color << " is disqualified" << std::endl;
    }

    void Logger::on_draw()
    {
        std::cout << "[CHESS]: draw !" << std::endl;
    }
} // namespace listener
