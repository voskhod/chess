#!/bin/sh

YELLOW='\033[0;33m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

engine="$1"/chessengine

[ ! -f "$engine" ] && echo -e "[${YELLOW}ERROR${NC}] compile before, you fuckin moron" && exit 1

[ -f out ] && echo -e "[${YELLOW}ERROR${NC}] out file already exist" && exit 1

has_succeed=0

for filename in tests/pgn/*.pgn
do
    filename="${filename%.*}"
    ./"$engine" -l ./tests/libbasic-output.so --pgn "$filename".pgn > out

    diff out "$filename.out"
    if [[ "$?" -ne 0 ]]
    then
        echo -e "[${RED}FAILED${NC}] $(basename -- $filename)"
        has_succeed=1
    else
        echo -e "[${GREEN}PASSED${NC}] $(basename -- $filename)"
    fi

done

rm out
exit "$has_succeed"
