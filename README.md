# Chess

*Chess* is a chess engine with an AI made by Alexis Hard, Maxence Carling and I.
It supports the Universal Chess Interface (UCI) so it can be used with a vast
majority of GUIs.

The engine can also check the validity of PGNs files and perform some perft tests.

There are still many improvements to do, especially with the AI (implementing full
bitboards, better check and mat detection, better board evaluation, quiescence
and iterative deepening).

# Build
```sh
mkdir build && cd build && cmake ..
make
```

# Run the tests
Only in debug mode
```sh
make && ./test
./tools/pgn-testsuite.sh
./tools/perft-testsuite.py tests/perft .
```
