#include <gtest/gtest.h>
#include <utility>


#include "../src/chessboard.hh"
#include "../src/position.hh"


using namespace board;


class ChessboardTest : public ::testing::Test
{
public:
    Chessboard board;
    friend class Chessboard;
};

TEST_F(ChessboardTest, BasicSet)
{
    board::Position pos (File::C, Rank::TWO);
    board.set_piece(pos, {PieceType::PAWN, Color::WHITE});
    auto result = board[pos];
    ASSERT_TRUE(result);
    ASSERT_EQ((*result).first, PieceType::PAWN);
    ASSERT_EQ((*result).second, Color::WHITE);
}

TEST_F(ChessboardTest, BasicSet2)
{
    board::Position pos(File::A, Rank::FOUR);
    board.set_piece(pos, {PieceType::QUEEN, Color::BLACK});
    auto result = board[pos];
    ASSERT_TRUE(result);
    ASSERT_EQ((*result).first, PieceType::QUEEN);
    ASSERT_EQ((*result).second, Color::BLACK);
}

TEST_F(ChessboardTest, Init)
{
    auto r = board[Position(File::E, Rank::EIGHT)];
    ASSERT_TRUE(r);
    ASSERT_EQ((*r).first, PieceType::KING);
    ASSERT_EQ((*r).second, Color::BLACK);
}

TEST_F(ChessboardTest, Init2)
{
    auto r = board[Position(File::B, Rank::ONE)];
    ASSERT_TRUE(r);
    ASSERT_EQ((*r).first, PieceType::KNIGHT);
    ASSERT_EQ((*r).second, Color::WHITE);
}

TEST_F(ChessboardTest, Init3)
{
    auto r = board[Position(File::G, Rank::TWO)];
    ASSERT_TRUE(r);
    ASSERT_EQ((*r).first, PieceType::PAWN);
    ASSERT_EQ((*r).second, Color::WHITE);
}

TEST_F(ChessboardTest, Init4)
{
    auto r = board[Position(File::A, Rank::EIGHT)];
    ASSERT_TRUE(r);
    ASSERT_EQ((*r).first, PieceType::ROOK);
    ASSERT_EQ((*r).second, Color::BLACK);
}

TEST_F(ChessboardTest, check_occupied_white)
{
    ASSERT_TRUE(board.is_occupied(Position(File::A, Rank::ONE)));
}

TEST_F(ChessboardTest, check_occupied_black)
{
    ASSERT_TRUE(board.is_occupied(Position(File::G, Rank::EIGHT)));
}

TEST_F(ChessboardTest, check_not_occupied)
{
    ASSERT_FALSE(board.is_occupied(Position(File::C, Rank::FOUR)));
}

TEST_F(ChessboardTest, check_color_white)
{
    ASSERT_EQ(Color::WHITE, board.get_color_at(Position(File::A, Rank::ONE)));
}

TEST_F(ChessboardTest, check_color_black)
{
    ASSERT_EQ(Color::BLACK, board.get_color_at(Position(File::G, Rank::EIGHT)));
}

TEST_F(ChessboardTest, check_color_none)
{
    ASSERT_EQ(std::nullopt, board.get_color_at(Position(File::C, Rank::FIVE)));
}

