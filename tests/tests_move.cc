#include <gtest/gtest.h>


#include "../src/move.hh"



using namespace board;


TEST(MoveTest, Simple)
{
    Position pos1(File::B, Rank::ONE);
    Position pos2(File::E, Rank::EIGHT);
    Move move = Move(PieceType::QUEEN, pos1, pos2);

    ASSERT_EQ(move.get_piece(), PieceType::QUEEN);
    ASSERT_EQ(move.from(), pos1);
    ASSERT_EQ(move.to(), pos2);

    ASSERT_EQ(move.promotion(), std::nullopt);
    ASSERT_EQ(move.capture(), std::nullopt);

    ASSERT_FALSE(move.double_pawn_push());
    ASSERT_FALSE(move.kingside_castling());
    ASSERT_FALSE(move.queenside_castling());
    ASSERT_FALSE(move.en_passant());
}


TEST(MoveTest, Promotion)
{
    Position pos1(File::B, Rank::ONE);
    Position pos2(File::E, Rank::EIGHT);

    Move move = Move(PieceType::PAWN, pos1, pos2);
    move.set_promotion(PieceType::QUEEN);

    ASSERT_EQ(move.get_piece(), PieceType::PAWN);
    ASSERT_EQ(move.from(), pos1);
    ASSERT_EQ(move.to(), pos2);

    ASSERT_TRUE(move.promotion());
    ASSERT_EQ(*move.promotion(), PieceType::QUEEN);
    ASSERT_EQ(move.capture(), std::nullopt);

    ASSERT_FALSE(move.double_pawn_push());
    ASSERT_FALSE(move.kingside_castling());
    ASSERT_FALSE(move.queenside_castling());
    ASSERT_FALSE(move.en_passant());
}


TEST(MoveTest, Capture)
{
    Position pos1(File::B, Rank::ONE);
    Position pos2(File::E, Rank::EIGHT);

    Move move = Move(PieceType::PAWN, pos1, pos2);
    move.set_capture(PieceType::QUEEN);

    ASSERT_EQ(move.get_piece(), PieceType::PAWN);
    ASSERT_EQ(move.from(), pos1);
    ASSERT_EQ(move.to(), pos2);

    ASSERT_EQ(move.promotion(), std::nullopt);
    ASSERT_TRUE(move.capture());
    ASSERT_EQ(move.capture(), PieceType::QUEEN);

    ASSERT_FALSE(move.double_pawn_push());
    ASSERT_FALSE(move.kingside_castling());
    ASSERT_FALSE(move.queenside_castling());
    ASSERT_FALSE(move.en_passant());
}


TEST(MoveTest, DoublePush)
{
    Position pos1(File::B, Rank::ONE);
    Position pos2(File::E, Rank::EIGHT);

    Move move = Move(PieceType::PAWN, pos1, pos2);
    move.set_double_pawn_push();

    ASSERT_EQ(move.get_piece(), PieceType::PAWN);
    ASSERT_EQ(move.from(), pos1);
    ASSERT_EQ(move.to(), pos2);

    ASSERT_EQ(move.promotion(), std::nullopt);
    ASSERT_EQ(move.capture(), std::nullopt);

    ASSERT_TRUE(move.double_pawn_push());
    ASSERT_FALSE(move.kingside_castling());
    ASSERT_FALSE(move.queenside_castling());
    ASSERT_FALSE(move.en_passant());
}


TEST(MoveTest, KingCastling)
{
    Position pos1(File::B, Rank::ONE);
    Position pos2(File::E, Rank::EIGHT);

    Move move = Move(PieceType::PAWN, pos1, pos2);
    move.set_kingside_castling();

    ASSERT_EQ(move.get_piece(), PieceType::PAWN);
    ASSERT_EQ(move.from(), pos1);
    ASSERT_EQ(move.to(), pos2);

    ASSERT_EQ(move.promotion(), std::nullopt);
    ASSERT_EQ(move.capture(), std::nullopt);

    ASSERT_FALSE(move.double_pawn_push());
    ASSERT_TRUE(move.kingside_castling());
    ASSERT_FALSE(move.queenside_castling());
    ASSERT_FALSE(move.en_passant());
}


TEST(MoveTest, QueenCastling)
{
    Position pos1(File::B, Rank::ONE);
    Position pos2(File::E, Rank::EIGHT);

    Move move = Move(PieceType::PAWN, pos1, pos2);
    move.set_queenside_castling();

    ASSERT_EQ(move.get_piece(), PieceType::PAWN);
    ASSERT_EQ(move.from(), pos1);
    ASSERT_EQ(move.to(), pos2);

    ASSERT_EQ(move.promotion(), std::nullopt);
    ASSERT_EQ(move.capture(), std::nullopt);

    ASSERT_FALSE(move.double_pawn_push());
    ASSERT_FALSE(move.kingside_castling());
    ASSERT_TRUE(move.queenside_castling());
    ASSERT_FALSE(move.en_passant());
}


TEST(MoveTest, EnPassant)
{
    Position pos1(File::B, Rank::ONE);
    Position pos2(File::E, Rank::EIGHT);

    Move move = Move(PieceType::PAWN, pos1, pos2);
    move.set_en_passant();

    ASSERT_EQ(move.get_piece(), PieceType::PAWN);
    ASSERT_EQ(move.from(), pos1);
    ASSERT_EQ(move.to(), pos2);

    ASSERT_EQ(move.promotion(), std::nullopt);
    ASSERT_EQ(move.capture(), std::nullopt);

    ASSERT_FALSE(move.double_pawn_push());
    ASSERT_FALSE(move.kingside_castling());
    ASSERT_FALSE(move.queenside_castling());
    ASSERT_TRUE(move.en_passant());
}

