#include <gtest/gtest.h>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <string>

#include "../src/fen_rank.hh"
#include "../src/fen_object.hh"


using namespace board;


TEST(Perft, ConstructorSimple)
{
    FenRank fen = FenRank("PPPPPPPP");
    
    auto result = fen[File::A];
    ASSERT_TRUE(result);
    ASSERT_EQ((*result).first, PieceType::PAWN);
    ASSERT_EQ((*result).second, Color::WHITE);
}


TEST(Perft, ConstructorColor)
{
    FenRank fen = FenRank("pppppppp");
    
    auto result = fen[File::A];
    ASSERT_TRUE(result);
    ASSERT_EQ((*result).first, PieceType::PAWN);
    ASSERT_EQ((*result).second, Color::BLACK);
}


TEST(Perft, HardConstructor)
{
    FenRank fen = FenRank("rnbq1k1r");
    auto result = fen[File::A];
    ASSERT_TRUE(result);
    ASSERT_EQ((*result).first, PieceType::ROOK);
    ASSERT_EQ((*result).second, Color::BLACK);
}


TEST(Perft, HardSecond)
{
    FenRank fen = FenRank("2p5");
    auto result = fen[File::C];
    ASSERT_TRUE(result);
    ASSERT_EQ((*result).first, PieceType::PAWN);
    ASSERT_EQ((*result).second, Color::BLACK);
}


TEST(Perft, Empty)
{
    FenRank fen = FenRank("8");
    auto result = fen[File::D];
    ASSERT_FALSE(result);
}



TEST(Perft, FenObjectImplemtation)
{
    std::string input = "rnbq1k1r/pp1Pbppp/2p5/8/2B5/8/PPP1NnPP/RNBQK2R w KQ - 1 8";
    std::vector<std::string> inputs;
    boost::split(inputs, input, boost::is_any_of(" "));
    FenObject fen = FenObject::parse(inputs);
    ASSERT_EQ(Color::WHITE, fen.side_to_move_get());
    ASSERT_EQ('K', fen.castling_get()[0]);
    ASSERT_EQ('Q', fen.castling_get()[1]);
    ASSERT_FALSE(fen.en_passant_target_get());
}
